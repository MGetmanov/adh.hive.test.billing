package adh.cloud.billing.cassandra.repository.account;

import adh.cloud.billing.cassandra.commons.IFRepository;
import adh.cloud.billing.commons.Account;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.utils.UUIDs;

import java.util.Iterator;
import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class AccountRepository implements IFRepository<Account> {
    private static final String TABLE_NAME = "account";
    private static final String FIELDS = "accountId, idSourcedOperation, timeStampOperation, timeStampCreated";
    private final Session session;

    public AccountRepository(Session session) {
        this.session = session;
    }

    @Override
    public String tableName() {
        return TABLE_NAME;
    }

    @Override
    public AccountRepository createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append(tableName())
                .append("(accountId uuid PRIMARY KEY, idSourcedOperation bigint,timeStampOperation bigint, timeStampCreated bigint);");
        final String query = sb.toString();
        session.execute(query);

        return this;
    }

    @Override
    public UUID insert(Account account) {
        UUID id = UUIDs.timeBased();
        StringBuilder sb = new StringBuilder("INSERT INTO ").append(tableName())
                .append("(").append(FIELDS).append(") ")
                .append("VALUES (").append(id).append(", ")
                .append(account.idSourcedOperation()).append(", ")
                .append(account.timeStampOperation()).append(", ")
                .append(System.currentTimeMillis()).append(")")
                .append(" IF NOT EXISTS ;");
        final String query = sb.toString();
        if (session.execute(query).one().getBool(0)) {
            return id;
        }
        return null;
    }

    @Override
    public boolean forceInsert(Account account) {
        throw new UnsupportedOperationException("Надо дописать");
    }


    @Override
    public boolean insertBatch(Iterator<Account> values) {
        StringBuilder sb = new StringBuilder("BEGIN BATCH ");
        while (values.hasNext()) {
            Account account = values.next();
            sb.append("INSERT INTO ").append(tableName())
                    .append("(").append(FIELDS).append(") ")
                    .append("VALUES (")
                    .append(account.accountId()).append(", '")
                    .append(account.idSourcedOperation()).append("', '")
                    .append(account.timeStampOperation()).append("')")
                    .append(System.currentTimeMillis()).append(")")
                    .append(" IF NOT EXISTS ;");
        }
        sb.append("APPLY BATCH;");
        final String query = sb.toString();
        session.execute(query);
        return session.execute(query).one().getBool(0);
    }

    @Override
    public Stream<Account> selectAll() {
        Statement select = QueryBuilder.select()
                .from(tableName());
        ResultSet rs = session.execute(select);
        return StreamSupport.stream(rs.spliterator(), false).map(this::buildAccount);
    }

    @Override
    public Account selectById(UUID uuid) {
        Statement select = QueryBuilder.select()
                .from(tableName())
                .where(QueryBuilder.eq("accountId", uuid));
        return buildAccount(session.execute(select).one());
    }

    private Account buildAccount(Row r) {
        if (r == null) {
            return null;
        }
        return Account.build(r.getUUID("accountId"),
                r.getLong("idSourcedOperation"),
                r.getLong("timeStampCreated"),
                r.getLong("timeStampOperation")
        );
    }

    @Override
    public AccountRepository deleteTable() {
        StringBuilder sb = new StringBuilder("DROP TABLE IF EXISTS ").append(tableName());
        final String query = sb.toString();
        session.execute(query);
        return this;
    }
}
