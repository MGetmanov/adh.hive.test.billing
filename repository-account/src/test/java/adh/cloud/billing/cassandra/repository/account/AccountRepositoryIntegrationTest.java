package adh.cloud.billing.cassandra.repository.account;

import adh.cloud.billing.commons.Account;
import adh.cloud.utils.test.proto.AbstractCassTest;
import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class AccountRepositoryIntegrationTest extends AbstractCassTest<AccountRepository> {


    @Test
    public void tableStructureChecker() {
        ResultSet result = session.execute("SELECT * FROM " + KEYSPACE_NAME + "." + repository.tableName() + ";");
        List<String> columnNames = result.getColumnDefinitions().asList().stream()
                .map(ColumnDefinitions.Definition::getName).collect(Collectors.toList());
        assertEquals(columnNames.toString(), columnNames.size(), 4);
        assertTrue(columnNames.toString(), columnNames.contains("accountId".toLowerCase()));
        assertTrue(columnNames.toString(), columnNames.contains("idSourcedOperation".toLowerCase()));
        assertTrue(columnNames.toString(), columnNames.contains("timeStampOperation".toLowerCase()));
        assertTrue(columnNames.toString(), columnNames.contains("timeStampCreated".toLowerCase()));
    }

    @Test
    public void when_insert_then_create_new_row() {
        ResultSet result = session.execute("SELECT COUNT(*) FROM " + KEYSPACE_NAME + "." + repository.tableName() + ";");
        long startEntries = result.one().getLong(0);
        repository.insert(Account.build(null, 0, 0, 0));
        result = session.execute("SELECT COUNT(*) FROM " + KEYSPACE_NAME + "." + repository.tableName() + ";");
        long resultEntries = result.one().getLong(0);
        Assert.assertEquals(startEntries + 1, resultEntries);
    }

    @Test
    public void when_insertNewAccount_then_canRead_ThisAccount() {
        UUID uuid = repository.insert(Account.build(null, 0, 0, 0));
        Account account = repository.selectById(uuid);
        assertEquals(uuid, account.accountId());
        assertNotEquals(0, account.timeStampCreated());
    }

    @Override
    protected AccountRepository initRepository(Session session) {
        return new AccountRepository(session);
    }
}
